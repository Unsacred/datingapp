from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db.models import PointField

GENDER_TYPES = [
    ('M', 'Мужской'),
    ('F', 'Женский')
]


class Client(AbstractUser):
    image = models.ImageField(upload_to='photo/', default=None, verbose_name='Фото')
    gender = models.CharField(max_length=10,
                              null=False,
                              blank=False,
                              choices=GENDER_TYPES,
                              verbose_name='Пол',
                              db_index=True)
    
    location = PointField(db_index=True, geography=True, default=None, null=True)

    def save(self, *args, **kwargs):
        self.username = self.email
        return super().save(*args, **kwargs)


class ClientMatch(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='client')
    match = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='match')

    class Meta:
        unique_together = [('client', 'match')]
