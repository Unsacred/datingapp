import json

from django.contrib.auth.hashers import make_password
from django.contrib.gis.geos import Point
from django.contrib.gis.geos.geometry import GEOSException

from django.utils.translation import gettext as _

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_gis.serializers import GeometryField

from .models import Client, ClientMatch


class GeometryPointFieldSerializerFields(GeometryField):

    def to_internal_value(self, value: str):
        if value.count(',') != 1:
            raise ValidationError(
                _("Enter the co-ordinates in (latitude,longitude). Ex-12,13")
            )
        lat, lon = value.split(',')

        try:
            lat = float(lat)
        except ValueError:
            raise ValidationError(
                _("Latitude should be float format")
            )

        try:
            lon = float(lon)
        except ValueError:
            raise ValidationError(
                _("Longtitude should be float format")
            )

        return Point(lon, lat)

    def to_representation(self, value):
        value = super().to_representation(value)
        # change to compatible with google map
        return {
            "type": "Point",
            "coordinates": [
                value['coordinates'][1], value['coordinates'][0]
            ]
        }

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['first_name', 'last_name', 'image', 'gender', 'email', 'password', 'location']

    password = serializers.CharField(write_only=True, style={'input_type': 'password', 'placeholder': 'password'})
    location = GeometryPointFieldSerializerFields(required=False)

    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))

        return super().create(validated_data)


class ClientMatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientMatch
        fields = ['client', 'match']
