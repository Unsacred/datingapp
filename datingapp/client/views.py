from django.shortcuts import get_object_or_404
from django.http import HttpResponseBadRequest, HttpResponse, HttpResponseForbidden
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.files import File

from io import BytesIO
from PIL import Image, ImageFile

from .models import Client


@login_required
def handle_image(request, pk):
    client = get_object_or_404(Client, id=pk)

    if request.user != client and not request.user.is_staff:
        return HttpResponseForbidden(b'Access Denied')

    if 'image' not in request.FILES:
        return HttpResponseBadRequest(b'Expected image is not in request')

    watermark = Image.open(f'{settings.BASE_DIR}/static/watermark.jpg')

    parser = ImageFile.Parser()
    parser.feed(request.FILES['image'].read())
    image = parser.close()

    image.paste(watermark, (0, 0))
    bi = BytesIO()
    image.save(bi, format='jpeg')
    client.image.save(f'{pk}.jpeg', File(bi))
    client.save()
    return HttpResponse(b'Image saved')










