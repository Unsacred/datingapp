from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

from io import BytesIO
from PIL import Image

from .models import Client


class ClientTest(TestCase):
    def test_image_upload(self):
        client = Client.objects.create(
            first_name='bob',
            last_name='doe',
            gender='M',
            email='bob@bob.com',
            password='qjhasdbf4353Ad',
        )
        self.client.force_login(client)
        url = reverse('upload', args=[client.id])
        bi = BytesIO()
        data = Image.open(f'{settings.BASE_DIR}/client/testdata/test_image.jpg')
        data.save(bi, format='jpeg')
        bi.seek(0)
        file = SimpleUploadedFile('test_image', content=bi.read(), content_type='image/jpg')
        response = self.client.post(url, data={'image': file})
        self.assertEqual(response.status_code, 200, response.content)

        client = Client.objects.get(id=client.id)
        with open(f'{settings.BASE_DIR}/client/testdata/watermarked_image.jpeg', mode='rb') as fp:
            wi_bytes = fp.read()
        self.assertEqual(wi_bytes, client.image.read())
        client.image.delete()

    def test_access_to_upload(self):
        client = Client.objects.create(
            first_name='bob',
            last_name='doe',
            gender='M',
            email='bob@bob.com',
            password='qjhasdbf4353Ad',
        )
        wrong_client = Client.objects.create(
            first_name='Eve',
            last_name='doe',
            gender='M',
            email='eve@eve.com',
            password='qjhasdbf4353Ad',
        )
        self.client.force_login(wrong_client)
        url = reverse('upload', args=[client.id])
        response = self.client.post(url)
        self.assertEqual(response.status_code, 403, response.content)

    def test_filter_client(self):
        fist_name_list = ['bob', 'eve', 'joe']
        last_name_list = ['smith', 'doe', 'abra']
        gender_list = ['M', 'F']

        for first_name in fist_name_list:
            for last_name in last_name_list:
                for gender in gender_list:
                    Client.objects.create(first_name=first_name,
                                          last_name=last_name,
                                          gender=gender,
                                          email=f'{first_name}{gender}@{last_name}.com',
                                          password='qjhasdbf4353Ad',
                                          )
        response = self.client.get('/api/list', data={'gender': 'M'})
        self.assertEqual(response.status_code, 200)
        response_clients = response.json()
        self.assertEqual(len(response_clients), 9)
        self.assertFalse([c for c in response_clients if c['gender'] != 'M'])

        response = self.client.get('/api/list', data={'first_name': 'bob'})
        self.assertEqual(response.status_code, 200)
        response_clients = response.json()
        self.assertEqual(len(response_clients), 6)
        self.assertFalse([c for c in response_clients if c['first_name'] != 'bob'])

        response = self.client.get('/api/list', data={'last_name': 'doe'})
        self.assertEqual(response.status_code, 200)
        response_clients = response.json()
        self.assertEqual(len(response_clients), 6)
        self.assertFalse([c for c in response_clients if c['last_name'] != 'doe'])

        response = self.client.get('/api/list', data={'gender': 'M',
                                                      'first_name': 'bob',
                                                      'last_name': 'doe'})
        self.assertEqual(response.status_code, 200)
        response_clients = response.json()
        self.assertEqual(len(response_clients), 1)
        self.assertFalse([c for c in response_clients if c['gender'] != 'M'
                          and c['last_name'] != 'doe'
                          and c['first_name'] != 'bob'])



