from django.test import TestCase
from django.contrib.gis.geos import Point
from django.core import mail

from client.models import Client

from unittest.mock import Mock

mail.send_mail = Mock


class CreateClientTest(TestCase):
    def test_create_client(self):
        client_info = {
            'first_name': 'bob',
            'last_name': 'doe',
            'gender': 'M',
            'email': 'bob@bob.com',
            'password': 'qjhasdbf4353Ad',
        }
        response = self.client.post('/api/clients/create', client_info)
        self.assertEqual(response.status_code, 201)

    def test_match_client(self):
        client = Client.objects.create(
            first_name='bob',
            last_name='doe',
            gender='M',
            email='bob@bob.com',
            password='qjhasdbf4353Ad',
        )
        match = Client.objects.create(
            first_name='Eve',
            last_name='brown',
            gender='F',
            email='eve@eve.com',
            password='qjhasdbf4353Aasdd',
        )
        self.client.force_login(client)
        response = self.client.post(f'/api/clients/{client.id}/match', data={'client': client.id, 'match': match.id})
        self.assertEqual(response.status_code, 201)

        self.client.force_login(match)
        response = self.client.post(f'/api/clients/{match.id}/match', data={'client': match.id, 'match': client.id})
        self.assertEqual(response.status_code, 201)
        self.assertContains(response, client.email, status_code=201)
        self.assertTrue(mail.send_mail.called)

    def test_nearby_clients(self):
        client = Client.objects.create(
            first_name='bob',
            last_name='doe',
            gender='M',
            email='bob@bob.com',
            password='qjhasdbf4353Ad',
            location=Point(0, 0),
        )
        near_client = Client.objects.create(
            first_name='Eve',
            last_name='doe',
            gender='M',
            email='eve@eve.com',
            password='qjhasdbf4353Ad',
            location=Point(0, 4),
        )
        far_client = Client.objects.create(
            first_name='Jane',
            last_name='doe',
            gender='M',
            email='jane@jane.com',
            password='qjhasdbf4353Adasd',
            location=Point(0, 10),
        )
        self.client.force_login(client)
        responce = self.client.get('/api/list', data={'distance': 600})
        self.assertEqual(responce.status_code, 200)
        client_list_json = responce.json()
        self.assertEqual(len(client_list_json), 1)
        self.assertEqual(client_list_json[0]['first_name'], 'Eve', client_list_json)
