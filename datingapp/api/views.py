from django.http import JsonResponse
from django.conf import settings
from django.contrib.gis.measure import Distance
from django.core.mail import send_mail

from django_filters import rest_framework as filters
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response

from client.filters import ClientFilter
from client.models import ClientMatch, Client
from client.serializers import ClientSerializer, ClientMatchSerializer


class ClientView(APIView):
    parser_classes = (MultiPartParser,)

    def post(self, request):
        serializer = ClientSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        serializer.save()
        return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)


class ClientMatchView(APIView):
    def post(self, request, pk):
        serializer = ClientMatchSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        link = serializer.save()
        reverse_links = (
            ClientMatch.objects
            .filter(match_id=link.client_id, client_id=link.match_id)
            .prefetch_related('client', 'match')
        )

        if not reverse_links:
            return Response(status=status.HTTP_201_CREATED)

        client = reverse_links[0].match
        match = reverse_links[0].client
        send_mail(
            'Найдено совпадение',
            f'Вы понравились {client.first_name}! Почта участника: {client.email}',
            settings.DEFAULT_FROM_EMAIL,
            [match.email]
        )
        send_mail(
            'Найдено совпадение',
            f'Вы понравились {match.first_name}! Почта участника: {match.email}',
            settings.DEFAULT_FROM_EMAIL,
            [client.email]
        )
        return JsonResponse({'email': match.email}, status=status.HTTP_201_CREATED)


class ClientListView(ListAPIView):
    queryset = Client.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ClientFilter
    serializer_class = ClientSerializer

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        if 'distance' in self.request.GET:
            return qs.filter(location__distance_lt=(
                self.request.user.location,
                Distance(km=int(self.request.GET['distance'])),
            )).exclude(id=self.request.user.id)
        return qs
