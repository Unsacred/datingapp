from django.urls import path
from .views import ClientView, ClientMatchView, ClientListView

app_name = 'api'

urlpatterns = [
    path('clients/create', ClientView.as_view(), name='create'),
    path('clients/<int:pk>/match', ClientMatchView.as_view(), name='match'),
    path('list', ClientListView.as_view(), name='list'),
]
