from django.contrib import admin
from django.urls import path, include

from client.views import handle_image

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('api.urls', 'api')),
    path('client/<int:pk>/', handle_image, name='upload')
]
